
#include <iostream>
#include <time.h>

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int date = buf.tm_mday;
    int sum = 0;
    const int N = 5;
    int a[N] [N];
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            a[i][j] = i + j;
            std::cout << a[i][j] << " ";
        }
        std::cout << "\n";
    }
    int ost = date % N;
    
    for (int j = 0; j < N; j++)
    {
        sum += a[ost][j];

    }
    std::cout << "Date%N line amount: " << sum;
}
